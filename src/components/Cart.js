import React, { Fragment } from 'react';
import { Table } from 'react-bootstrap';
import { sumBy } from 'lodash';
import ItemCart from '../common/ItemCart';


const Cart = ({
  selectProduct = [],
  ...other
}) => (
    <Fragment>
      <Table striped bordered condensed hover >
        <thead>
          <tr>
            <th>Item</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total price</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {selectProduct.map(val => <ItemCart key={val.id} cart={val} {...other} />)}
          <tr>
            <td colSpan={3}>Total</td>
            <td colSpan={2}>{sumBy(selectProduct, (pr) => (parseFloat(pr.total)))} USD</td>
          </tr>
        </tbody>
      </Table>
    </Fragment>
  )
export default Cart;
