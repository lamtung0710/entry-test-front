import React, { Component } from 'react';
import { get } from 'lodash';
import { Table } from 'react-bootstrap';

class Review extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectProduct: []
    };
    this.onSort = this.onSort.bind(this);
  }

  componentDidMount() {
    const { location: { state: { selectProduct } } } = this.props;
    this.setState({ selectProduct })
  }

  onSort(event, sortKey) {
    const { selectProduct } = this.state;
    selectProduct.sort((a, b) => String(a[sortKey]).localeCompare(String(b[sortKey])))
    this.setState({ selectProduct })
  }

  render() {
    const { selectProduct = [] } = this.state;

    return (
      <div>
        <Table responsive hover bsClass="table">
          <thead>
            <tr >
              <th>Id</th>
              <th onClick={e => this.onSort(e, 'productName')}>Name</th>
              <th onClick={e => this.onSort(e, 'price')}>Price</th>
              <th onClick={e => this.onSort(e, 'quantity')}>Quantity</th>
              <th onClick={e => this.onSort(e, 'total')}>Total price</th>
            </tr>
          </thead>
          <tbody>
            {selectProduct.map(val => (<tr key={val.id}>
              <td>{get(val, 'id', '-')}</td>
              <td>{get(val, 'productName', '-')}</td>
              <td>{get(val, 'price', '-')}</td>
              <td>{get(val, 'quantity', '-')}</td>
              <td>{get(val, 'total', '-')}</td>
            </tr>))}
          </tbody>
        </Table>
      </div>
    )
  }
}
export default Review;
