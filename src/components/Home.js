import React, { Component, Fragment } from 'react';
import Product from './Product';
import Cart from './Cart';
import Axios from 'axios';
import { findIndex } from 'lodash';
import { Link } from 'react-router-dom';


class Home extends Component {

  constructor(prop) {
    super(prop);
    this.state = {
      products: [],
      selectProduct: [],
    }
    this.onSelect = this.onSelect.bind(this);
    this.onMinus = this.onMinus.bind(this);
    this.onPlus = this.onPlus.bind(this);
    this.onRemove = this.onRemove.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  componentDidMount() {
    this.initData();
  }

  initData() {
    Axios.get('http://103.28.38.197:1338/api/product').then(({ data: { product = [] } }) => {
      this.setState({ products: product })
    });
  }


  onPlus(product) {
    const { selectProduct } = this.state;
    const newSelectProduct = selectProduct;
    const index = findIndex(newSelectProduct, (val) => val.id === product.id);
    if (index !== -1) {
      newSelectProduct[index].quantity = product.quantity + 1
      newSelectProduct[index].total = parseFloat(product.price) * (product.quantity + 1)
    }
    this.setState({ selectProduct: newSelectProduct });
  }

  onMinus(product) {
    const { selectProduct } = this.state;
    const newSelectProduct = selectProduct;
    const index = findIndex(newSelectProduct, (val) => val.id === product.id);
    const quantity = product.quantity - 1;
    if (index !== -1) {
      if (quantity) {
        newSelectProduct[index].quantity = quantity;
        newSelectProduct[index].total = parseFloat(product.price) * (product.quantity - 1)
      } else {
        newSelectProduct.splice(index, 1);
      }

    }
    this.setState({ selectProduct: newSelectProduct });
  }

  onChangeText(quantity, product) {
    const { selectProduct } = this.state;
    const newSelectProduct = selectProduct;
    const index = findIndex(newSelectProduct, (val) => val.id === product.id);

    if (index !== -1) {
      if (quantity) {
        newSelectProduct[index].quantity = quantity;
        newSelectProduct[index].total = parseFloat(product.price) * quantity;
      } else {
        newSelectProduct.splice(index, 1);
      }

    }
    this.setState({ selectProduct: newSelectProduct });
  }

  onRemove(product) {
    const { selectProduct } = this.state;
    const newSelectProduct = selectProduct;
    const index = findIndex(newSelectProduct, (val) => val.id === product.id);
    if (index !== -1) {
      newSelectProduct.splice(index, 1);
    }
    this.setState({ selectProduct: newSelectProduct });
  }

  onSelect(product) {
    const { selectProduct } = this.state;
    const newSelectProduct = selectProduct;

    if (findIndex(newSelectProduct, (val) => val.id === product.id) === -1) {
      newSelectProduct.push(product);
    }
    this.setState({ selectProduct: newSelectProduct });
  }

  render() {
    const { products, selectProduct } = this.state;
    return (
      <Fragment>
        <Cart
          selectProduct={selectProduct}
          onMinus={this.onMinus}
          onPlus={this.onPlus}
          onChangeText={this.onChangeText}
          onRemove={this.onRemove} />
        <Link className='btn success' to={{ pathname: '/review', state: { selectProduct } }} >Submit</Link>
        <Product
          products={products}
          onSelect={this.onSelect}
          selectProduct={selectProduct}
        />

      </Fragment>
    )
  }
}
export default Home;
