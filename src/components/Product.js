import React, { Fragment } from 'react';
import '../css/itemProduct.css';
import { Table } from 'react-bootstrap';
import { findIndex } from 'lodash';
import ItemProduct from '../common/ItemsProduct';


const Product = ({
  products = [],
  selectProduct = [],
  ...other
}) => (
    <Fragment>
      <Table responsive hover bsClass="table">
        <thead>
          <tr >
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {products.map(val => <ItemProduct
            key={val.id}
            product={val}
            isSelect={findIndex(selectProduct, (item) => item.id === val.id) !== -1}
            {...other} />)}
        </tbody>
      </Table>
    </Fragment>
  )
export default Product;
