import React, { Component } from 'react';
import './css/styles.css';
import Home from './components/Home';
import Review from './components/Review';
import { BrowserRouter as Router, Route, } from 'react-router-dom';
class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={Home} />
          <Route path="/review" component={Review} />
        </div>
      </Router>
    );
  }
}

export default App;
