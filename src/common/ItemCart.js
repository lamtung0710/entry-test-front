import React from 'react';
import { get } from 'lodash';
import { Button, } from 'react-bootstrap';


const Cart = ({
	cart,
	onPlus = () => { },
	onMinus = () => { },
	onRemove = () => { },
	onChangeText = () => { },
	value = ''
}) => (
		<tr >
			<td>{get(cart, 'productName', '-')}</td>
			<td>{get(cart, 'price', '-')} USD</td>
			<td>
				<div className='total-price'>
					<Button bsStyle='success' className='btn-cart' onClick={() => onMinus(cart)} bsSize='xsmall'>-</Button>
					<input className='input-text' type='text' value={get(cart, 'quantity', 0)} onChange={({ target: { value } }) => {
						const quantity = get(cart, 'quantity', 0);
						const newValue = parseInt(String(value).replace(/\D/g, '') || quantity, 10);
						onChangeText(newValue, cart);
					}} />
					<Button bsStyle='success' className='btn-cart' onClick={() => onPlus(cart)} bsSize='xsmall'>+</Button>
				</div>
			</td>
			<td>{get(cart, 'total', 0)} USD</td>
			<td>
				<Button onClick={() => onRemove(cart)} bsSize='xsmall'>-</Button>
			</td>
		</tr>)

export default Cart;