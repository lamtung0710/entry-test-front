import React from 'react';
import { get } from 'lodash';
import { Button } from 'react-bootstrap';


const Product = ({
  onSelect = () => { },
  product = {},
  isSelect = false
}) => (
    <tr>
      <td>{get(product, 'id', '-')}</td>
      <td>{get(product, 'productName', '-')}</td>
      <td>{get(product, 'price', '-')}</td>
      <td >
        <Button
          bsClass='btn btn-add-product'
          disabled={isSelect}
          onClick={() => onSelect({ ...product, quantity: 1, total: parseFloat(product.price) })}>
          Add to cart
        </Button>
      </td>
    </tr>
  )
export default Product